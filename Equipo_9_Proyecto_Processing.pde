
/*
CITM - UPC - Multimedia, 1r curso
Matematicas
Equipo 9:
Integrantes:
  Juan Carlos Boixeda Rull
  Guillem Coll Belda
  Ferran Folch Gazquez
  Marina Ramirez Garcia
Profesor:
  Julian Pfeifle
Fecha:
  07/06/2021
*/

int g=255,r=255,b=255;
void setup() {
  size(1000, 1000);
  background(80);
  smooth();
  noStroke();
}

void draw(){
  int invFc = frameCount-255;
  ellipse(width*0.5, height*0.5,50,50);
 if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255-invFc,255-invFc,255);
}
  pushMatrix();
  translate(width*0.5, height*0.5);
rotate(radians(frameCount * 2  % 360));

  ellipse(50, 50, 1,100); 
  ellipse(100, 100, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255-invFc,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.2, height*0.2);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255-invFc,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.2, height*0.8);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255-invFc,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.8, height*0.2);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255-invFc,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.8, height*0.8);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.5, height*0.8);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255,255-invFc,255-invFc);
}

 pushMatrix();
  translate(width*0.8, height*0.5);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.5, height*0.2);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();

if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255,255-invFc,255-invFc);
}

  pushMatrix();
  translate(width*0.2, height*0.5);
rotate(radians(frameCount * 2  % 360));

  ellipse(30, 30, 1,100); 
  ellipse(90, 90, 1,100); 
  ellipse(150, 150, 1,100);
  popMatrix();
/*
if (frameCount<=255){
    fill(frameCount +1, frameCount +1, frameCount +1);
   }
    else{fill(255-invFc,255-invFc,255-invFc);
}*/
}
