String helpText=""; 


float circle1X=110, circle1Y=300, c1s=4;
float circle2X=135, circle2Y=235, c2sX=3.57, c2sY=1.65;
float circle3X=167, circle3Y=167, c3s=2.82;
float circle4X=235, circle4Y=135, c4sX=1.65, c4sY=3.57;
float circle5X=300, circle5Y=110, c5s=4;
float circle6X=365, circle6Y=135, c6sX=1.65, c6sY=3.57;
float circle7X=433, circle7Y=167, c7s=2.82;
float circle8X=465, circle8Y=235, c8sX=3.57, c8sY=1.65;

int cont=0;
int startTime;

void setup() {
  startTime = millis();
  size(600,600);
  
}

void movecircle1(){
  /*fill(50,50,50);
    line(100,300,500,300);*/
  
  stroke(0);
    fill(50,50,50);
    ellipse(circle1X,circle1Y,20,20);
    
    
    
  circle1X += c1s;
  if(circle1X>490 || circle1X<110)
  {            
    c1s= c1s * (-1);
    //background(50,50,50);
    fill(50,50,50);
    ellipse(300,300,450,450);
  } 
}
  void movecircle2(){  
   /*fill  (190, 10, 164);
   line(118,218,482,382);
   //line(125,225,475,375);*/
   

    
    stroke(0);
   fill  (1190, 10, 164);
   ellipse(circle2X,circle2Y,20,20);
  circle2X += c2sX;
  circle2Y += c2sY;
  if((circle2X<130 && circle2Y<230) || (circle2X>465 && circle2Y>365))
  {
    c2sX= c2sX * (-1);
    c2sY= c2sY * (-1);
    
    //background(1190, 10, 164);
    fill(1190, 10, 164);
    ellipse(300,300,450,450);
  } 
  
  
  
}
void movecircle3(){ 
  //line(158,158,442,442);
  stroke(0);
  fill(255,0,0); 
  ellipse(circle3X,circle3Y,20,20);
  
  circle3X += c3s;
  circle3Y += c3s;
  if((circle3X<165 && circle3Y<165) || (circle3X>437 && circle3Y>437))
  {
    c3s= c3s * (-1);
    //background(255,0,0);
    fill(255,0,0);
    ellipse(300,300,450,450);
  }  
}
  void movecircle4(){  
    //line(218,118,382,482);
   stroke(0);
   fill(255,164,32);
   ellipse(circle4X,circle4Y,20,20);
  circle4X += c4sX;
  circle4Y += c4sY;
  if((circle4X<230 && circle4Y<130) || (circle4X>365 && circle4Y>465))
  {
    c4sX= c4sX * (-1);
    c4sY= c4sY * (-1);
    //background(255,164,32);
    fill(255,164,32);
    ellipse(300,300,450,450);
  }    
}
void movecircle5(){
  //fill(243,218,011);
  //line(300,100,300,500);
  
  stroke(0);
    fill(243,218,011);
    ellipse(circle5X,circle5Y,20,20);
  circle5Y += c5s;
  if(circle5Y>490 || circle5Y<110)
  {            
    c5s= c5s * (-1);
    //background(243,218,011);
    fill(243,218,011);
    ellipse(300,300,450,450);
  } 
}
  void movecircle6(){
    //line(382,118,218,482);
   stroke(0);
   fill(0,255,64);
   ellipse(circle6X,circle6Y,20,20);
  circle6X -= c6sX;
  circle6Y += c6sY;
  if((circle6X<235 && circle6Y>465) || (circle6X>370 && circle6Y<130))
  {
    c6sX= c6sX * (-1);
    c6sY= c6sY * (-1);
    //background(0,255,64);
    fill(0,255,64);
    ellipse(300,300,450,450);
  }    
}
void movecircle7(){ 
  //line(442,158,158,442);
  stroke(0);
  fill(0,64,255); 
  ellipse(circle7X,circle7Y,20,20);
  circle7X -= c7s;
  circle7Y += c7s;
  if((circle7X>435 && circle7Y<165) || (circle7X<165 && circle7Y>437))
  {
    c7s= c7s * (-1);
    //background(0,64,255);
    fill(0,64,255);
    ellipse(300,300,450,450);
  }  
}

  void movecircle8(){
    //line(482,218,118,382);
   stroke(0);
   fill(100,0,230);
   ellipse(circle8X,circle8Y,20,20);
  circle8X -= c8sX;
  circle8Y += c8sY;
  if((circle8X>470 && circle8Y<230) || (circle8X<135 && circle8Y>365))
  {
    c8sX= c8sX * (-1);
    c8sY= c8sY * (-1);
    //background(100,0,230);
    fill(100,0,230);
    ellipse(300,300,450,450);
  }    
}


void draw() {
  
  //----- Base -----
  //background(255);
  /*stroke(0);
  fill(128);
  rect(0,0,600,600);*/
  stroke(0);
  fill(255);
  ellipse(300,300,400,400);
  
    //----- Moving dots -----
  cont+=1;  
  
  if (millis() - startTime > 0){
  movecircle1();
  }
  if (millis() - startTime > 300){
  movecircle2();
  }
  if (millis() - startTime > 500){
  movecircle3();
  }
  if (millis() - startTime > 700){
  movecircle4();
  }
  if (millis() - startTime > 900){
  movecircle5();
  }
  if (millis() - startTime > 1100){
  movecircle6();
  }
  if (millis() - startTime > 1300){
  movecircle7();
  }
  if (millis() - startTime > 1500){
  movecircle8();
  }
  
  

  
    //showRuler();
}




// ---------------------------------------------------------------

/*void showRuler() {

  int fullLengthLine=28; 

  // X-Ruler 
  stroke(0, 0, 255);  // BLUE  
  fill(0, 0, 255); 
  line(0, 0, width, 0); 
  // if (false) 
  for (int x=0; x<=width; x+=50) {
    if (x%100==0) {
      // at 0, 100, 200, 300..... we show text and line
      if (x==width) {
        line(x-1, 0, x-1, fullLengthLine); 
        text(x, x-30, fullLengthLine+15);
      } else if (x==0) {
        line(1, 0, 1, fullLengthLine); 
        text(x, x+2, fullLengthLine+15);
      } else {
        line(x, 0, x, fullLengthLine); 
        text(x, x-11, fullLengthLine+15);
      }
    } else if (x%50==0) {
      // at 50, 150, 250.... we only show a shorter line
      line(x, 0, x, fullLengthLine/2);
    }
  }//for

  // ------------------------------------------------------

  // Y-Ruler 
  stroke(255, 0, 0); // RED 
  fill(255, 0, 0); 
  line(0, 0, 
    0, height); 
  for (int y=0; y<=height; y+=50) {
    if (y%100==0) {
      // at 0, 100, 200, 300..... we show text and line
      if (y==height) {
        line(0, height-1, fullLengthLine, height-1); 
        text(y, 
          30, height-7);
      } else if (y==0) {
        line(0, 1, fullLengthLine, 1);
        text(y, 30, 18);
      } else {
        // normal line and text  
        line(0, y, fullLengthLine, y); 
        text(y, fullLengthLine+3, y+4);
      }
    } else if (y%50==0) {
      // at 50, 150, 250.... we only show a shorter line 
      line(0, y, fullLengthLine/2, y);
    }
  }//for

  /*helpText="The screen is "
    +width
    +" wide and \n"
    +height 
    + " high.\nMouse is at "
    +mouseX+"(x), "
    +mouseY+"(y).";

  showHelpText();
}// func

// ------------------------------------------
// Minor Tools 

void showHelpText() {
  // Yellow box
  fill(#F6FA1C); // Yellow
  stroke(0);     // Black
  rect(width-225, 63, 
    210, 62);

  // Black text
  fill(0); // Black
  textSize(12); 
  text(helpText, 
    width-220, 80);
  textSize(14);
}*/
  
  
