float t;

void setup() { 
  fullScreen();  
  colorMode(RGB);
}

void draw() {  
  
  t = mouseX*1.0/width;  
  background(130,70,240);  
  pushMatrix();  
  translate(width/2, height/2); 
  strokeWeight(7.5); 
  pushMatrix();
  translate(2, 3); 
  drawCircles();
  popMatrix();  
  stroke(250,60,170);  
  strokeWeight(6);
  fill(14,85,255);  
  drawCircles();
  popMatrix();
 
}

//////////////////////////////////////////////////////////////////////////////

int samplesPerFrame = 4;
int numFrames = 144;        
float shutterAngle = .5;

int N = 360;
int n = 7;
float os;
float R = 300, r;
float th;
color bg = color(42, 40, 38);

void drawCircle(float q){  
  beginShape();  
  for (int i=0; i<N; i++) {  
    th = i*TWO_PI/N;
    os = map(cos(th-TWO_PI*t), -1, 1, 0, 1);
    os = 0.125*pow(os, 2.75);
    r = R*(1+os*cos(n*th + 1.5*TWO_PI*t + q));
    vertex(r*sin(th), -r*cos(th));  
  }
  endShape(CLOSE);
}

void drawCircles() { 
  drawCircle(0);
  drawCircle(PI);
}
