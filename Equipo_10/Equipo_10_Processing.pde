int angulo, angulo2, change2=40, change=30, change3=20, change4=10, change5=0;

void setup (){
 fullScreen ();
 colorMode (HSB,400);
}

void draw (){
  background (change2,400,400);
  change2++;
  if(change2==400){
    change2=0;
  }
  fill(change,400,400);
  change++;
  if(change==400){
    change=0;
  }
  stroke (0);
  strokeWeight(5);
  circle (width/2, height/2, height-30);
  
  fill(change3,400,400);
  change3++;
  if(change3==400){
    change3=0;
  }
  stroke (0);
  strokeWeight(5);
  translate (width/2,height/2);
  rotate (radians(angulo));
  angulo++;
  circle (0,height/2-310, height-500);
  
  fill(change4,400,400);
  change4++;
  if(change4==400){
    change4=0;
  }
  stroke (0);
  strokeWeight(5);
  translate (0,height/2-310);
  rotate (radians(angulo2));
  angulo2++;
  circle (0,height/4-120, height-800);
  
  fill(change5,400,400);
  change5++;
  if(change5==400){
    change5=0;
  }
  stroke (0);
  strokeWeight(5);
  translate (0,height/4-120);
  rotate (radians(angulo2));
  angulo2++;
  circle (0,height/8-40, height-990);
}
