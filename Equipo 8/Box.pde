class Box{
  PVector pos;
  float len;
  
  Box(float x, float y, float z, float len_){
    pos = new PVector(x,y,z);
    len = len_;
  }
  
  void show(){
    fill(255);
    stroke(0);
    strokeWeight(8);
    
    pushMatrix();
    translate(pos.x,pos.y,pos.z);
    
    beginShape(QUADS);
    float r = len/2;
    
    fill(colors[0]);
    vertex(-r,-r,-r);
    vertex(r,-r,-r);
    vertex(r,r,-r);
    vertex(-r,r,-r);
  
    fill(colors[1]);
    vertex(-r,-r,r);
    vertex(r,-r,r);
    vertex(r,r,r);
    vertex(-r,r,r);
   
    fill(colors[2]);
    vertex(-r,-r,-r);
    vertex(-r,r,-r);
    vertex(-r,r,r);
    vertex(-r,-r,r);
    
    fill(colors[3]);
    vertex(r,-r,-r);
    vertex(r,r,-r);
    vertex(r,r,r);
    vertex(r,-r,r);
    
    fill(colors[4]);
    vertex(-r,-r,-r);
    vertex(r,-r,-r);
    vertex(r,-r,r);
    vertex(-r,-r,r);
    
    fill(colors[5]);
    vertex(-r,r,-r);
    vertex(r,r,-r);
    vertex(r,r,r);
    vertex(-r,r,r);
    
    endShape();
    popMatrix();
  }
  
}
