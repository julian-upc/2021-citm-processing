import peasy.*;
PeasyCam cam;

int c[] = {0, 100, 200, 300, 400};
int rand=int(random(0, 4));

color[] colors = {#FFFFFF, #FFFF00, #FF0000, #FFA500, #00FF00, #0000FF};
int dim = 3;
Box[][][] cube = new Box[dim][dim][dim];

int i=0, j=50;
boolean up=false;

int r=10, rot=10, tate=0, b=0;

void setup() {
  fullScreen(P3D);
  colorMode(HSB, 500);
  cam = new PeasyCam(this, 300);

  for (int i = 0; i<dim; i++) {
    for (int j = 0; j<dim; j++) {
      for (int k = 0; k<dim; k++) {
        float len = 70;
        float off = (dim*len)/2 - len/2;
        float x = len*i-off;
        float y = len*j-off;
        float z = len*k-off;
        cube[i][j][k] = new Box(x, y, z, len);
      }
    }
  }
}

void draw() {

  switch(rand) {
  case 0: // piramide
    background(100);
    translate(0, 0, -25);
    if (mousePressed) {
      for (int i=0; i<5; i++) {
        c[i]++;
        if (c[i]>=500) {
          c[i]=0;
        }
      }
    }

    noStroke();
    lights();
    pointLight(255, 255, 255, 100, 100, 100);
    fill(c[0], 255, 255);
    square(-50, -50, 100);

    push();
    rotateY(radians(90));
    translate(0, 0, -50);
    fill(c[1], 500, 500);
    beginShape(TRIANGLE);
    vertex(0, -50, 0);
    vertex(0, 50, 0);
    vertex(-100, 0, 50);
    endShape();
    pop();

    push();
    rotateY(radians(-90));
    translate(0, 0, -50);
    fill(c[2], 500, 500);
    beginShape(TRIANGLE);
    vertex(0, -50, 0);
    vertex(0, 50, 0);
    vertex(100, 0, 50);
    endShape();
    pop();

    push();
    rotateX(radians(90));
    translate(0, 0, -50);
    fill(c[3], 500, 500);
    beginShape(TRIANGLE);
    vertex(-50, 0, 0);
    vertex(50, 0, 0);
    vertex(0, 100, 50);
    endShape();
    pop();

    push();
    rotateX(radians(-90));
    translate(0, 0, -50);
    fill(c[4], 500, 500);
    beginShape(TRIANGLE);
    vertex(-50, 0, 0);
    vertex(50, 0, 0);
    vertex(0, -100, 50);
    endShape();
    pop();
    break;

  case 1: // rubick's cube
    background(50);

    for (int i = 0; i<dim; i++) {
      for (int j = 0; j<dim; j++) {
        for (int k = 0; k<dim; k++) {

          cube[i][j][k].show();
        }
      }
    }
    break;

  case 2: // formas geometricas
    background(0);
    noStroke();
    lights();
    if (up) {
      j++;
      if (j>50) {
        up=!up;
      }
    } else {
      j--;
      if (j<=0) {
        up=!up;
      }
    }

    for (int i=0; i<=360; i+=10) {

      push();
      translate(300, 0, 0);
      push();
      rotateZ(radians(i));
      translate(j, 0, -50);
      fill(500, 300, 500);
      beginShape(TRIANGLE);
      vertex(0, -50, 0);
      vertex(0, 50, 0);
      vertex(-100, 0, 50);
      endShape();
      pop();

      push();
      rotateZ(radians(i));
      translate(j, 0, -50);
      fill(500, 300, 500);
      beginShape(TRIANGLE);
      vertex(0, -50, 0);
      vertex(0, 50, 0);
      vertex(-100, 0, -50);
      endShape();
      pop();
      pop();


      push();
      rotateY(radians(i));
      translate(0, 0, -j);
      fill(100, 300, 500);
      beginShape(TRIANGLE);
      vertex(0, -50, 0);
      vertex(0, 50, 0);
      vertex(-100, 0, 50);
      endShape();
      pop();


      push();
      rotateY(radians(i));
      translate(0, 0, -j);
      fill(100, 300, 500);
      beginShape(TRIANGLE);
      vertex(0, -50, 0);
      vertex(0, 50, 0);
      vertex(100, 0, 50);
      endShape();
      pop();

      push();
      translate(-300, 0, 0);
      push();
      rotateX(radians(i));
      translate(0, 0, -j);
      fill(300, 300, 500);
      beginShape(TRIANGLE);
      vertex(0, -50, 0);
      vertex(0, 50, 0);
      vertex(-100, 0, 50);
      endShape();
      pop();


      push();
      rotateX(radians(i));
      translate(0, 0, -j);
      fill(300, 300, 500);
      beginShape(TRIANGLE);
      vertex(0, -50, 0);
      vertex(0, 50, 0);
      vertex(100, 0, 50);
      endShape();
      pop();
      pop();
    }
    break;

  case 3: // pendulos
    background(0);
    strokeWeight(1);
    lights();
    push();
    stroke(5);
    rotate(radians(tate/2));
    fill(100, 500, 500);
    sphere(r);
    stroke(255,0,500);
    line(0, 2*r, 0, -2*r);
    push();
    translate(0, 2*r);
    stroke(255,0,500);
    sphere(rot/10);
    pop();
    push();
    translate(0, -2*r);
    fill(255,0,500);
    stroke(255,0,500);
    box(r/2);
    pop();
    pop();
    push();
    translate (0, -2*r);
    rotate(radians(-tate/2));
    fill(200, 100, 500);
    sphere(r);
    stroke(255,0,500);
    line(0, -2*r, 0, 2*r);
    push();
    translate(0, 2*r);
    fill(255,0,500);
    box(r/2);
    pop();
    translate(0, -2*r);
    stroke(255,0,500);
    sphere(rot/10);
    pop();

    if (b==0) {
      tate++;
    }
    if (tate>580) {

      b=1;
    }
    if (tate<140) {
      b=0;
    }

    if (b==1) {
      tate--;
    }
    if (r<500) {

      r++;
    }
    if (rot<250) {
      rot++;
    }
    break;
  }
}

void keyPressed() {
  if (key=='r') {
    rand = int(random(0, 4));
    print("Mostrando proyecto numero: " + r + "\n");
  }
}
