class Planeta {
  int r;
  float v, o, a;
  int num_planeta;
  PShape p, l;

  Planeta(int radio, int tam, float vel_orbital, PImage tex, int num_planeta_) {
    r = radio;
    v = random(TWO_PI);
    o = vel_orbital;
    num_planeta = num_planeta_;
    p = createShape(SPHERE, tam);
    p.setTexture(tex);

    if (num_planeta==3) { // tierra
      l = createShape(SPHERE, 4);
      l.setTexture(loadImage("moon.jpg"));
    } else if (num_planeta==6) { // saturno
      a = radians(random(10, 30));
    }
  }

  void render() {
    push();
    rotate(v);
    translate(r, 0, 0);
    push();
    rotateX(radians(-90));
    shape(p);
    pop();
    if (num_planeta==3) { // tierra
      push();
      rotate(v*13);
      translate(25, 0, 0);
      shape(l);
      pop();
    } else if (num_planeta==6) { // saturno
      push();
      scale(0.9);
      rotateX(a);
      image(loadImage("saturnring.png"), -152.5, -152.5);
      pop();
    }

    pop();

    v = v + o;
  }
}
