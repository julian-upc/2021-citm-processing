import ddf.minim.analysis.*;
import ddf.minim.*;

Minim minim;
AudioPlayer player;
FFT fft;
int[][] colo=new int[300][3];
PImage leftHalf;

void setup(){
  size(700, 400);
  noCursor();
  minim = new Minim(this);
  player = minim.loadFile("believer.mp3",1024);
  fft = new FFT(player.bufferSize(), player.sampleRate());
  player.play();
  smooth();
}
  
void draw(){
  background(0);
  stroke(255,50,100,175);

   if (player.isPlaying())
  {
   text("Espacio para pausar \ny cambiar de sonido.", 75, 50);
  }
  else
  {
   text("1(Canción) / 2 (DO) / 3 (RE) / 4 (MI)\n   5 (FA) / 6 (SOL) / 7 (LA) / 8 (SI)\n\n   Espacio para volver a reproducir", 25, 50);
  }
  
  fft.forward(player.mix);

  for(int i = 0; i < fft.specSize(); i++) { 
    line(i, height, i, height - fft.getBand(i)*45);
  } 
    leftHalf = get(0, 0, width/2, height);
    translate(width, 0);
    scale(-1, 1);
    image(leftHalf, 0, 0);

  for(int i = 0; i < player.left.size() - 1; i++){ 
    stroke(200,0,255,200);
    strokeWeight(2);
    line(i, 150 + player.left.get(i)*50, i+1, 150 + player.left.get(i+1)*50); 
    line(i, 250 + player.right.get(i)*50, i+1, 250 + player.right.get(i+1)*50); 
  }
}

void keyPressed()
{
  if(keyPressed && player.isPlaying()){
    
  }
  else
  {
    if(key == '1'){
      player = minim.loadFile("believer.mp3",1024);
    }
    if(key == '2'){
      player = minim.loadFile("do261.mp3",1024);
    }
    if(key == '3'){
      player = minim.loadFile("re294.mp3",1024);
    }
    if(key == '4'){
      player = minim.loadFile("mi330.mp3",1024);
    }
    if(key == '5'){
      player = minim.loadFile("fa349.mp3",1024);
    }
    if(key == '6'){
      player = minim.loadFile("sol392.mp3",1024);
    }
    if(key == '7'){
      player = minim.loadFile("la440.mp3",1024);
    }
    if(key == '8'){
      player = minim.loadFile("si494.mp3",1024);
    }
  }
  if (key == ' ' && player.isPlaying() )
  {
   player.pause();
  }
  else if (key == ' ')
  {
    player.play();
  }
}
