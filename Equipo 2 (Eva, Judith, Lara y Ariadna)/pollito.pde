void setup() {
  size(800, 800);
}

void draw() {
  background(135,206,235);
  translate(-width/2, -height/2); // mueve el origen hacia arriba y hacia la izquerda
  translate(mouseX, mouseY); // sigue el mouse
  
  // pico
  noStroke();
  fill(244, 70, 17);
  triangle(93, 386, 134, 420, 136, 361);
  triangle(504,403,536,431,538,377);
  
  //barriga
  fill(240,165,32); 
  noStroke();
  ellipse(250,580,300,300);
  ellipse(580,500,150,150);
  
  //cabeza
  fill(225,225,0);
  ellipse(200,400,150,150);
  ellipse(560,405,75,75);
  
  //ala
  noStroke();
  fill(244, 70, 17);
  triangle(175, 580, 325, 580, 250, 665);
  triangle(557, 500, 602, 500, 580, 537);
  
  //iris
  fill(0,0,0);
  ellipse(200,400,50,50);
  ellipse(560,405,25,25);
  
    //reflejo
  fill(240,248,255);
  ellipse(200,400-15,20,20);
  ellipse(560,405-17.5+10,10,10);

}
