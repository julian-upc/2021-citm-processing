class Bola {
  int r, d, d2, a=0;
  float m;
  color c;

  Bola(int radio, int dist, float multi) {
    r=radio;
    d=dist;
    m=multi; //mnultiplicador velocitat
 
    c=int (random(0,10));
  }
  
  void move(){
    push();
    translate(width/2,height/2);
    a+=m; 
    rotate(radians(a));
    
    stroke(200, 200, 200);

    strokeWeight(5);
    fill(200+c,c,20*c);
    circle(100,d,r);
    pop();
  }
}
